Sujets de TP
============

Version : 2018-02-04

Les sujets de TP sont mis à jour chaque année.


Consignes
---------

### Un élève pour un sujet

Pour la matière EDL il n'y a pas de binome pour les TP.
Chaque élève doit choisir son/ses sujets de TP seul, doit réaliser son/ses
sujets de TP seul, doit effectuer son/ses *Pull Request* (PR) seul.

Les élèves sont autorisés à demander de l'aide à leurs camarades de classe, mais
l'évaluation est individuelle.

### La référence

Les élèves doivent se servir du paquet [mcf](https://github.com/madarche/mcf-js)
comme référence, comme exemple pour toutes les bonnes pratiques à jour à
suivre.

### Bonnes pratiques

* Les modifications dans un projet doivent être réalisées :

    * dans un commit unique (utiliser `git rebase` comme vu en cours pour
      fusionner des commits successifs si vous êtes amenés à faire plusieurs
      commits)

    * dans une branche dédiée (la branche doit avoir un nom porteur de sens,
      adapté, et tout en minuscule), par exemple `fix-column-check`,
      `feat-dependency-update`, `feat-eslint-switch` etc.

* Les commits doivent impérativement suivre les *règles des commits Git* :
    * [article 1](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
    * [article2](https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message)

* Quand dans un projet des fonctionnalités sont ajoutées (par exemple ajout de
nouvelles méthodes, etc.) ou que le comportement est modifié (modification de
méthodes, changement des options par défaut, etc.) if faut mettre à jour la
documentation et, si il y en a, généralement dans un répertoire `test` ou
`tests`) les tests unitaires du projet.

### Validation de vos PR

Les élèves ne doivent **jamais** envoyer une PR à un projet sans avoir fait
valider cette PR dans leur fourche (fork) du projet par un professeur.


Notation des TP
---------------

### Critères de notation

* Conformité de la PR aux *règles des commits Git*
  cf. [Bonnes pratiques](#bonnes-pratiques)
* Empathie avec les auteurs du logiciel (se renseigner sur le projet et lire les
  PR effectuées par d'autres, certains auteurs aiment un style très strict,
  d'autres un style décontracté, etc.)
* Qualité de la rédaction du message en anglais
* Le *merge* d'une *Pull Request* (PR), c'est à dire l'approbation de la PR par
  l'auteur du logiciel

### Rapport de TP

Il faut écrire un rapport minimaliste et l'envoyer par courriel aux professeurs.
Le rapport peut être un simple fichier en Markdown, ou en HTML, ou
éventuellement un fichier ODT.

Ce rapport doit juste indiquer :

* l'URL du compte GitHub/GitLab/autre utilisé par l'élève
* le ou les URL des tickets et de la ou des *Pull Requests*/*Merge Requests* en
  indiquant pour chaque :
    * ce qui a été réalisé par l'élève
      (environ 1 paragraphe)
    * ce qui accepté/refusé par le(s) mainteneur(s) du projet
      (environ 1 paragraphe)

Les professeurs n'iront pas deviner où se trouvent les contributions des élèves,
c'est uniquement sur la base de ce rapport que les contributions seront lues et
évaluées.


Sujets
------

Les sujets ci-dessous sont proposés.

Choisir un projet distribué suivant une licence libre sur une forge logicielle
collaborative comme https://framagit.org/ , https://gitlab.com/explore/groups ,
ou https://github.com/ et y réaliser des contributions significatives du même
niveau de difficulté que les autres sujets ci-dessus.

Le sujet choisi par l'élève devra être validé par le professeur avant que
l'élève ne commence son travail de contribution.


[Watch](https://github.com/mikeal/watch)
----------------------------------------

Résoudre le ticket [Create automated tests](https://github.com/mikeal/watch/issues/87)

[chai-http](https://github.com/chaijs/chai-http)
------------------------------------------------

* Ajout des badges (npm, dependencies, devDependencies) => Attribué à KSA

* Ajout de ESLint => Attribué à CRA

* Ajout de ESLint dans les tests => Attribué à CRA


[CookieJar](https://github.com/bmeck/node-cookiejar)
----------------------------------------------------

* Documenter la méthode `CookieAccessInfo.All` => Attribué à SBE

* Passage à ESLint à la place de JShint => Attribué à SBE

* Ajouter la possibilité que le constructeur de `Cookie` accepte un objet
  `{name, value, expiration_date, path, domain, secure, httponly}` comme argument

* Ajout d'un build d'intégration continue avec le badge associé

    Actuellement il n'y a pas de build automatique de l'intégration continue.
    Quand un build de l'intégration continue fonctionne, il y a un pictogramme
    vert qui indique `build passing` comme sur
    https://github.com/madarche/mcf-js .
    Pour que le build passe, il faut que l'auteur du projet se crée un compte
    sur http://travis-ci.org/ et autorise Travis à accéder à son compte GitHub
    et au dépôt Git du projet. Pour arriver à expliquer cela à l'auteur, il faut
    au préalable que l'étudiant réalise les manipulations de son côté et
    comprenne comment cela fonctionne en mettant en place l'intégration continue
    sur son projet fourché (fork) de manière à avoir un `build passing`.

* Ajout de la couverture de tests avec ajout du badge coverage


[Nunjucks](https://github.com/mozilla/nunjucks)
----------------------------------------------------

* Dans le templating, ajouter le support de
  l'[opératieur `in`](http://jinja.pocoo.org/docs/2.9/templates/#other-operators) => Attribué à EEE


[Convict](https://github.com/mozilla/node-convict)
--------------------------------------------------

* Réorganiser les tests unitaires (pour les rendre plus clairs et plus
  maintenables, cela implique des renommages de fichiers et éventuellement la
  suppression ou la création de nouveaux fichiers de tests)


[repolint](https://github.com/madarche/npm-repolint)
----------------------------------------------------

* Ajouter l'intégration continue

* Générer les rapports dans un répertoire en dehors des sources

* Utiliser le paquet `yargs-parser` pour gérer les arguments passés en ligne de
  commande au programme


[F-Droid](https://gitlab.com/fdroid/fdroidclient/)
--------------------------------------------------

F-Droid est un magasin d'applications libres destiné à Android.  

Ils ont une liste de d'évolutions/anomalies destinées aux débutant [sur gitlab](https://gitlab.com/fdroid/fdroidclient/issues?label_name%5B%5D=first-timer).  

Choisir l'un des ticket et propser une Merge Request.  

Exemple : https://gitlab.com/fdroid/fdroidclient/issues/1119

[Libxmljs](https://github.com/libxmljs/libxmljs)
----------------------------------------------------

* Ajout de la couverture de tests avec ajout du badge coverage => Attribué à HJA


[SSPK](https://github.com/e-henry/sspk)
----------------------------------------------------

* Valider les comptes github des entrées du fichier => Attribué à ONB
* Valider la clef SSH des entrées du fichier (simple format ou appel
à ssh-keygen)

[insert-css](https://github.com/substack/insert-css)
----------------------------------------------------

* Ajout du badge npm, en reprise de la PR
  https://github.com/substack/insert-css/pull/30 faite précédemment par un autre
  élève => Attribué à RCA

* Ajout de ESLint dans les tests => Attribué à ANA

* Ajout de la couverture de tests avec ajout du badge coverage


[Npm-check-github-stars](https://github.com/OlivierCoilland/npm-check-github-stars)
-----------------------------------------------------------------------------------

* Ajout de tests fonctionnels => Attribué à AAL

* Ajout des badges (npm, dependencies, devDependencies) => Attribué à AAI

* Ajout de la couverture de tests avec ajout du badge coverage


[Simple-cookie](https://github.com/juji/simple-cookie)
------------------------------------------------------

* Ajout de la couverture de tests avec ajout du badge coverage => Attribué à YMA


[Mocha](https://github.com/mochajs/mochajs.github.io)
------------------------------------------------------

* Documenter dans https://mochajs.org/ l'utilisation de « `mocha` vs `_mocha` ».
  Se baser sur la pratique et la compréhension du code https://github.com/mochajs/mocha

  Faire une nouvelle PR en se basant sur le texte suivant fait dans une PR
  non-aboutie d'un élève d'une année précédente
  https://github.com/mochajs/old-mochajs-site/pull/77/commits/3f7ccfaa446ddc07b5e2db44c52fc5d4e463d338#diff-d680e8a854a7cbad6d490c445cba2ebaR1236


[Tracer](https://github.com/baryon/tracer)
------------------------------------------------------

* MAJ devDependencies => Attribué à KKH


[RandomColor](https://github.com/davidmerfield/RandomColor)
-----------------------------------------------------------

* Réaliser les points *To do*



[Hide-Address-Bar](https://github.com/scottjehl/Hide-Address-Bar)
-----------------------------------------------------------------

* Publication de ce code sur NPM (s’inspirer de la PR
  https://github.com/joequery/Stupid-Table-Plugin/pull/158)


[iOS-Orientationchange-Fix](https://github.com/scottjehl/iOS-Orientationchange-Fix)
-----------------------------------------------------------------------------------

* Publication de ce code sur NPM (s’inspirer de la PR
  https://github.com/joequery/Stupid-Table-Plugin/pull/158)


[Fast-csv](https://www.npmjs.com/package/fast-csv)
------------------------------------------------------

* Faire que l'option headers=Array renvoie une erreur si la première ligne du
  fichier CSV ne correspond pas à l'Array fourni (C'est peut-être corrigé avec
  les versions récentes : à valider)

* Passage à ESLint à la place de JSHint


[calc-polyfill](https://github.com/closingtag/calc-polyfill)
------------------------------------------------------------

* Réaliser les points *To Do*


[Scripto](https://github.com/e-henry/scripto)
---------------------------------------------------

* Effectuer une tâche de la TODO

Implement the PATCH method => Attribué à MBA  
Add a periodicity to script (hourly, daily, weekly, monthly), allowing to emit a warning if a script has not been launched for a moment => Attribué à MLA


[Atom](https://github.com/atom/atom)
---------------------------------------------------

* Prendre en charge une tâche consistant à améliorer
[la documentation](https://github.com/atom/atom/labels/documentation)


[node-gitlab](https://github.com/repo-utils/gitlab)
---------------------------------------------------

* Documenter l'utilisation de la pagination dans tous les appels
* Mettre à jour les dependencies et les devDependencies => Attribué à RCA
* Ajouter un fichier .editorconfig => Attribué à SND
* Passage de JSHint à ESLint => Attribué à SND
* Correction du build failing => Attribué à SND
* Ajouter la possibilité de faire un `client.projects.search({open_issues})` => Attribué à SND


[pdf-text-extract](https://github.com/nisaacson/pdf-text-extract)
-----------------------------------------------------------------

* Ajout de ESLint dans les tests => Attribué à AVA
* Ajout de la couverture de tests avec ajout du badge coverage
* Mise à jour des dependencies


["Good First Issue" sur Github](https://github.com/issues?utf8=%E2%9C%93&q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22+)
--------------------------------------------------------------------------------------------------------------------------------

Les mainteneurs qui souhaitent une aide extérieur peuvent ajouter un label "Good First Issue".
Ce label permet de distinguer les tickets qui sont simple à résoudre pour de nouveaux venus sur un projet.

* Choisir un sujet

[LibreOffice Easy Hacks](https://wiki.documentfoundation.org/Development/Easy_Hacks)
------------------------------------------------------------------------------------

Faire des modifications faciles sur LibreOffice

Ce sujet peut être choisi par plusieurs élèves, il suffit de choisir des tâches différentes.
