# Séance 2

## Utilisation des alias
En bash ou zsh on peut définir des alias ou des fonctions pour faciliter la saisie de commandes fastidieuses

### Définition d'un alias sous bash
Sous bash, il est souvent nécessaire d'ajouter des alias pour êytre plus productif.

Exemple d'un ~/.bash_aliases
```
alias p='cd ..'

# ps with special/custom display to especially display parent process ID
# (PPID).
alias psp='ps axo user,ruser,pid,ppid,args --sort ppid'

# some more ls aliases
alias ll='ls -l'
alias la='ls -la'
lt() {
    ls -lat $@|head
}

alias gst='git status --short -b'
alias gitg='gitg  2>/dev/null &'
alias git-remove-merged-branches='git branch --no-color --merged | command grep -vE "^(\*|\s*(master|develop|dev)\s*$)" | command xargs -n 1 git branch -d'
```

Pour que ce fichier soit effectif, il faut l'ajouter dans son .bashrc
```
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
```

Pour savoir la liste des alias disponible, il suffit de taper `alias` à l'invite du prompt

```
$ alias
alias grep='grep --colour=auto'
alias ll='ls -al'
alias ls='ls --color=auto'$ alias
alias grep='grep --colour=auto'
alias ll='ls -al'
alias ls='ls --color=auto'
```
### Définition d'un alias sous zsh
C'est pareil, sauf que oh-my-zsh en propose déjà une grande quantité (de grands classiques...)

```
$ alias
-='cd -'
...=../..
....=../../..
.....=../../../..
......=../../../../..
1='cd -'
2='cd -2'
3='cd -3'
4='cd -4'
5='cd -5'
6='cd -6'
7='cd -7'
8='cd -8'
....
```
(sortie tronquée)

Toutefois, ceux-là restent très utiles et peuvent être ajoutés au .zshrc

```
lt() {
    ls -lat $@|head
}

alias gst='git status --short -b'
alias gitg='gitg  2>/dev/null &'
alias git-remove-merged-branches='git branch --no-color --merged | command grep -vE "^(\*|\s*(master|develop|dev)\s*$)" | command xargs -n 1 git branch -d'
```
Le dernier est déjà fournit dans oh-my-zsh, mais sous un nom peu facile à retenir. Un alias sert aussi à donner un nom plus compréhensible à une commande.

### Fonction pour lister les repertoires les plus volumineux par ordre croissant

Ajoutez cette fonction dans votre .zshrc pour vous aider à faire le ménage dans votre répertoire personnel

```
# allow to display files and directories ordered by size
dush() {
    du -sh $@ | sort -h
}
```

## Intro à Markdown

Permet de formater du texte et le convertire en HTML par exemple.
Créez vous dés maintenant un répertoire dans lequel stoquer vos aides mémoires écrits en Markdown.

Exemple les commandes_shell.md , git.md, markdown.md

Atom permet d'avoir une prévisualisation automatique (package atom/markdown-preview activable avec ctrl-shift-M)

## Configurer git
- Editez votre ~/.gitignore

```
nogit
*.swp
```

`nogit` vous permet d'avoir des repértoire dans vos projets sans lesquels vous mettrez tout ce qui n'a pas besoin d'être dans le dépot de source
`*.swp` les fichiers temporaires de vim

- Editez votre ~/.gitconfig

```
[user]
	name = Prenom Nom
  email = prenom.nom@univ.fr

[color]
    ui = auto

[alias]
    lg = log --graph --pretty=tformat:'%Cred%h%Creset -%C(white)%d%Creset %s %Cgreen(%an %ar)%Creset'

[diff]
    mnemonicPrefix = true
    wordRegex = .
    renames = true
```


## Intro à git

- Testez git en local
- Cloner un dépot github sur le compte du professeur, et suivre les instructions.
