
Liste des paquets Debian dont nous avons besoin pour les TP :

* git
* gitk
* zsh
* vim
* netcat (pour tester les services ouverts)
* build-essential (pour compiler les paquets NPM contenant du code natif)
* curl
* less
* sed
* gawk
* gnupg2 (ou à défaut gnupg)
* python3
* pylint3
* python3-pep8
* virtualenv
* atom https://atom.io/download/deb
* docker.io (dans https://backports.debian.org/Packages/)

